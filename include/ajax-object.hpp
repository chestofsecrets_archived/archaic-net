/**
 * @project Archaic
 * @module Net
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-net/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef NET_AJAX_OBJECT
#define NET_AJAX_OBJECT

#include <map>
#include <string>
#include <any>

#include <archaic/core/event-object.hpp>

namespace Archaic
{
    class NetworkApplication;

    /**
     * Class to create an Http requests
     */
    class AjaxObject: public EventObject
    {
        std::map<std::string, std::any> params;
        std::string result;
        bool __done = false;
    public:
        /**
         * Sets the parameter of the request.
         * For now only url param is supported while data and type (GET, POST) params are planned to be implemented.
         * @param name
         * @param value
         * @return self as AjaxObject
         */
        AjaxObject* param(const std::string& name, const std::any& value);

        /**
         * Executes the request
         * @return self as AjaxObject
         */
        AjaxObject* exec();

        /**
         * Creates a "done" event which will be fired once the request stopped executing
         * @param job
         * @return self as AjaxObject
         */
        AjaxObject* done(const Job& job);

        /**
         * @return status of the request execution
         */
        bool done();

        /**
         * @return contents of buffer
         * May not be full if the method is not necessarily called after the request execution
         */
        std::string val();
    };
}


#endif //NET_AJAX_OBJECT

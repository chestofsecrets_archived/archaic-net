/**
 * @project Archaic
 * @module Net
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-net/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef NET_NETWORK_APPLICATION
#define NET_NETWORK_APPLICATION

#include <any>

#include <archaic/core/application.hpp>

#include "ajax-object.hpp"

namespace Archaic
{
    /**
     * Class-extension of Application which supports ajax requests and sockets
     */
    class NetworkApplication: public Application
    {
    public:
        AjaxObject* ajax(const std::string& url);
        AjaxObject* ajax(const std::initializer_list<std::any>& params);
    };
}


#endif //NET_NETWORK_APPLICATION

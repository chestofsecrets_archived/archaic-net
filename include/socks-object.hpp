/**
 * @project Archaic
 * @module Net
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-net/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef NET_SOCKS_OBJECT
#define NET_SOCKS_OBJECT

#include "socks-object_unix.hpp"

#endif //NET_SOCKS_OBJECT

/**
 * @project Archaic
 * @module Net
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-net/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef NET_SOCKS_OBJECT_UNIX
#define NET_SOCKS_OBJECT_UNIX
#ifdef unix

/*
 * The code below is not finished. It is supposed to declare SocksObject which
 * will be used to create client-server connections but its development is
 * suspended until the implementation of HashedObject used to create objects
 * with a unique hash name. Also sending primitives will not be implemented,
 * you will must create HashedObject extension that will contain needed primitive.
 *
#include <map>

#include <archaic/core/event-object.hpp>

namespace Archaic
{
    class SocksObject : public EventObject
    {
        addrinfo* localAddress = nullptr;
        int socket = -1;
        const unsigned bufferSize = 1024;
        std::string address;
        unsigned short port, localPort;
        std::map<std::string, int> connections;
        std::map<std::string, std::string> buffers;
    public:
        SocksObject(const std::string &address, unsigned short port, unsigned short localPort);
        ~SocksObject();
        */
/**
         * Starts the socket
         * @return null if failed to open socket or to connect to the server if needed. Else self as SocksObject
         *//*

        SocksObject* on();

        */
/**
         * Stops the socket
         * @return self as SocksObject
         *//*

        SocksObject* off();

        */
/**
         * Pushes entity to the buffer of given connection. If the buffer overflows sends it to its connection
         * @tparam T
         * @param entity
         * @param connection
         * @return self as SocksObject
         *//*

        template <typename T>
        SocksObject* push(T &entity, const std::string &connection);

        */
/**
         * Pushes entity to the buffers of given connections. If the buffer overflows sends it to its connection
         * @tparam T
         * @param entity
         * @param connections
         * @return self as SocksObject
         *//*

        template <typename T>
        SocksObject* push(T &entity, std::initializer_list<std::string> &cons);

        */
/**
         * Pushes entity to the buffers of all connections. If the buffer overflows sends it to its connection
         * @tparam T
         * @param entity
         * @return self as SocksObject
         *//*

        template <typename T>
        SocksObject* push(T &entity);

        */
/**
         * Forces the buffers of given connection to be sent to them and then cleaned
         * @param connection
         * @return self as SocksObject
         *//*

        SocksObject* send(const std::string &connection);

        */
/**
         * Forces the buffers of given connections to be sent to them and then cleaned
         * @param connections
         * @return self as SocksObject
         *//*

        SocksObject* send(std::initializer_list<std::string> &connections);

        */
/**
         * Forces the buffers of all connections to be sent to them and then cleaned
         * @return self as SocksObject
         *//*

        SocksObject* send();

        */
/**
         * Waits to receive the data from the given connection
         * @param connection
         * @param timeout in milliseconds. If equals 0, there will be no timeout
         * @return self as SocksObject
         *//*

        SocksObject* receive(const std::string &connection, unsigned long timeout = 0);

        */
/**
         * Waits to receive the data from any of given connections
         * @param connections
         * @param timeout in milliseconds. If equals 0, there will be no timeout
         * @return self as SocksObject
         *//*

        SocksObject* receive(std::initializer_list<std::string> &connections, unsigned long timeout = 0);

        */
/**
         * Waits to receive the data from any connection
         * @param timeout in milliseconds. If equals 0, there will be no timeout
         * @return self as SocksObject
         *//*

        SocksObject* receive(unsigned long timeout = 0);
    };
}

#include "socks-object_unix.inl"
*/

#endif
#endif //NET_SOCKS_OBJECT_UNIX
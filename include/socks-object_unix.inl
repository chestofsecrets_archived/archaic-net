/**
 * @project Archaic
 * @module Net
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-net/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef NET_SOCKS_OBJECT_UNIX_INL
#define NET_SOCKS_OBJECT_UNIX_INL

/*
 * The code below is not finished. It is supposed to declare SocksObject which
 * will be used to create client-server connections but its development is
 * suspended until the implementation of HashedObject used to create objects
 * with a unique hash name. Also sending primitives will not be implemented,
 * you will must create HashedObject extension that will contain needed primitive.
 *
#include <sstream>

using namespace Archaic;

template<typename T>
SocksObject* SocksObject::push(T &entity, const std::string &connection)
{
    std::ostringstream stream;
    stream << entity;
    auto str = stream.str();
    this->push(str, connection);
}

template<typename T>
SocksObject* SocksObject::push(T &entity, std::initializer_list<std::string> &cons)
{
    std::ostringstream stream;
    stream << entity;
    auto str = stream.str();
    this->push(str, cons);
}

template<typename T>
SocksObject* SocksObject::push(T &entity)
{
    std::ostringstream stream;
    stream << entity;
    auto str = stream.str();
    this->push(str);
}*/

#endif //NET_SOCKS_OBJECT_UNIX_INL

/**
 * @project Archaic
 * @module Net
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-net/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <curl/curl.h>
#include <curl/easy.h>

#include "ajax-object.hpp"

using namespace Archaic;

static size_t onRead(void *contents, size_t size, size_t nmemb, std::string *buffer)
{
    size *= nmemb;
    buffer->append(static_cast<const char *>(contents), size);

    return size;
}

AjaxObject* AjaxObject::exec()
{
    CURL *curl = curl_easy_init();

    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, std::any_cast<std::string>(this->params["url"]).c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, onRead);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &this->result);

        curl_easy_perform(curl);

        this->trigger("done");

        curl_easy_cleanup(curl);
    }
    this->vguard([this]() {
        this->__done = true;
    });

    return this;
}

AjaxObject* AjaxObject::param(const std::string &name, const std::any &value) {
    this->params[name] = value;

    return this;
}

AjaxObject* AjaxObject::done(const Job& job)
{
    this->on("done", job);

    return this;
}

bool AjaxObject::done()
{
    return this->guard<bool>([this] () {
        return this->__done;
    });
}

std::string AjaxObject::val()
{
    return std::any_cast<std::string>(result);
}

/**
 * @project Archaic
 * @module Net
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-net/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "ajax-object.hpp"

#include "network-application.hpp"

using namespace Archaic;

AjaxObject* NetworkApplication::ajax(const std::string& url)
{
    auto ajax = new AjaxObject;

    ajax->param("url", url);

    return ajax;
}

AjaxObject* NetworkApplication::ajax(const std::initializer_list<std::any>& params)
{
    auto ajax = new AjaxObject;

    for (auto iterator = std::rbegin(params); iterator != std::rend(params); iterator++) {
        auto pair = iterator++;

        if (pair == std::rend(params)) {
            ajax->param("url", *iterator);
            break;
        }
        ajax->param(std::any_cast<std::string>(*pair), *iterator);
    }

    return ajax;
}

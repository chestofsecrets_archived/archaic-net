/**
 * @project Archaic
 * @module Net
 * @author Maxim Vasiljev
 * @team Chest of Secrets
 * @url https://bitbucket.org/chestofsecrets/archaic-net/
 * @license Mozilla Public License 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifdef unix

/*
 * The code below is not finished. It is supposed to implement SocksObject which
 * will be used to create client-server connections but its development is
 * suspended until the implementation of HashedObject used to create objects
 * with a unique hash name. Also sending primitives will not be implemented,
 * you will must create HashedObject extension that will contain needed primitive.
 *
#include <sstream>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include "socks-object.hpp"

using namespace Archaic;

SocksObject::SocksObject(const std::string &address, unsigned short port, unsigned short localPort)
{
    this->address = address;
    this->port = port;
    this->localPort = port;
}

addrinfo* stringToAddress(const char* address, const char* service)
{
    addrinfo *hints = new addrinfo, *addr;

    hints->ai_family = AF_UNSPEC;
    hints->ai_socktype = SOCK_STREAM;
    if (!address) {
        hints->ai_flags = AI_PASSIVE;
    }

    ::getaddrinfo(address, service, hints, &addr);

    delete hints;

    return addr;
}

SocksObject* SocksObject::on()
{
    if ((!this->localPort && !this->port) || this->socket != -1) {
        return nullptr;
    }

    {
        std::ostringstream stream;
        stream << (this->localPort ?: this->port);

        this->localAddress = stringToAddress(nullptr, stream.str().c_str());
    }

    this->socket = ::socket(
        this->localAddress->ai_family,
        this->localAddress->ai_socktype,
        this->localAddress->ai_protocol
    );

    if (this->socket == -1) {
        return nullptr;
    }

    if (::bind(this->socket, this->localAddress->ai_addr, this->localAddress->ai_addrlen) == -1) {
        ::close(this->socket);
        return nullptr;
    }

    if (this->address.empty() || !this->port) {
        return this;
    }
    std::ostringstream stream;
    stream << this->port;

    auto connection = stringToAddress(this->address.c_str(), stream.str().c_str());

    if (::connect(this->socket, connection->ai_addr, connection->ai_addrlen) == -1) {
        freeaddrinfo(connection);
        ::close(this->socket);
        return nullptr;
    }
    this->buffers.emplace("", "");
    freeaddrinfo(connection);

    return this;
}

SocksObject* SocksObject::off()
{
    ::close(this->socket);
    freeaddrinfo(this->localAddress);

    return this;
}

template<>
SocksObject* SocksObject::push<std::string>(std::string &entity, const std::string &connection)
{
    auto position = this->buffers.find(connection);

    if (position == this->buffers.end()) {
        return this;
    }

    auto &buffer = position->second;

    auto bufferFreeSpace = buffer.size();
    auto entitySize = entity.size();
    auto entityBegin = 0;

    if (bufferFreeSpace + entitySize >= this->bufferSize) {
        buffer.append(entity, entityBegin, (entitySize - bufferFreeSpace) % this->bufferSize);

        this->send(connection);

        buffer.clear();
        entitySize -= this->bufferSize;
        entityBegin += this->bufferSize;

        while (entitySize >= this->bufferSize) {
            buffer = entity.substr(entityBegin, this->bufferSize);
        }
    }

    buffer.append(entity, entityBegin);

    return this;
}

template<>
SocksObject* SocksObject::push<Object*>(Object* &entity, const std::string &connection)
{
    auto binary = entity->toBinary();
    return this->push<std::string>(binary, connection);
}

template<>
SocksObject* SocksObject::push<std::string>(std::string &entity)
{
    for (auto &pair : this->connections) {
        this->push(entity, pair.first);
    }
    return this;
}

template<>
SocksObject* SocksObject::push<std::string>(std::string &entity, std::initializer_list<std::string> &cons)
{
    for (auto &connection : cons) {
        this->push(entity, connection);
    }
    return this;
}
*/

#endif
